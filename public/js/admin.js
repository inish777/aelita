$(document).ready(function () {
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                     'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                          'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    $("#add-event-textarea").ckeditor();
    $("#add-page-textarea").ckeditor();
    $("#gallery-description-textarea").ckeditor();
    $("#add-event-date-field").datepicker();

    $(".delete-event-button").on("click", function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        if(confirm("Удалить?")) {
            $.ajax($(this).attr("href"), {
                type: "POST",
                data: {
                    "_method" : "DELETE"
                },
                success: function() {
                    $(".event[data-id='" + id + "']").remove();
                }
            });
        }
    });

    $(".delete-page-button").on("click", function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        if(confirm("Удалить?")) {
            $.ajax($(this).attr("href"), {
                type: "POST",
                data: {
                    "_method" : "DELETE"
                },
                success: function() {
                    $(".page[data-id='" + id + "']").remove();
                }
            });
        }
    });
    
    $(".video-delete-button").on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      if(confirm("Удалить?")) {
        $.ajax($(this).attr("href"), {
            type: "POST",
            data: {
              "_method": "DELETE"
            },
            success: function() {
              $(".video[data-id='" + id + "']").remove();
            }
          }
        );
      }
    });
    
    $(".remove-report-button").on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      if(confirm("Удалить?")) {
        $.ajax($(this).attr("href"), {
            type: "POST",
            data: {
              "_method": "DELETE"
            },
            success: function() {
              $(".report[data-id='" + id + "']").remove();
            }
          }
        );
      }
    });
    
    $(".delete-gallery-button").on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      if(confirm("Удалить?")) {
        $.ajax($(this).attr("href"), {
            type: "POST",
            data: {
              "_method": "DELETE"
            },
            success: function() {
              $(".gallery[data-id='" + id + "']").remove();
            }
          }
        );
      }
    });
    
    $(".gallery-photo-delete-button").on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      if(confirm("Удалить?")) {
        $.ajax($(this).attr("href"), {
            type: "POST",
            data: {
              "_method": "DELETE"
            },
            success: function() {
              $(".gallery-photo[data-id='" + id + "']").remove();
            }
          }
        );
      }
    });
    
    $(".delete-url-button").on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      if(confirm("Удалить?")) {
        $.ajax($(this).attr("href"), {
            type: "POST",
            data: {
              "_method": "DELETE"
            },
            success: function() {
              $(".url[data-id='" + id + "']").remove();
            }
          }
        );
      }
    });

    $(".delete-banner-button").on("click", function(e) {
      e.preventDefault();
      var id = $(this).attr("data-id");
      if(confirm("Удалить?")) {
        $.ajax($(this).attr("href"), {
            type: "POST",
            data: {
              "_method": "DELETE"
            },
            success: function() {
              $(".banner[data-id='" + id + "']").remove();
            }
          }
        );
      }
    });
});