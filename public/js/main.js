(function () {
    var myMap,
        myPlacemark;
    ymaps.ready(function() {
        myMap = new ymaps.Map ("map", {
            center: [56.482796, 84.948853],
            zoom: 17
        });
        
        myPlacemark = new ymaps.Placemark([56.482677, 84.948466], { balloonContent: 'Зрелищный центр "Аэлита"'});
        myMap.geoObjects.add(myPlacemark);
    });
})();

$(document).ready(function() {
  $(".gallery-photo-link").colorbox({rel: 'gallery-photo', title: function() {
      return $(this).data('title');
    },
    current: 'Изображение {current} из {total}',
    width: '80%',
    height: '80%'
  });
  
  $(".report-image-link").colorbox({rel: 'report-photo', title: function() {
      return $(this).data('title');
    },
    current: 'Изображение {current} из {total}',
    width: '80%',
    height: '80%'
  });
});
