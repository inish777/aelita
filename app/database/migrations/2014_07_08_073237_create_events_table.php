<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('events', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->date('date');
            $table->string('time');
            $table->string('price');
            $table->boolean('18plus')->default(false);
            $table->string('type');

            $table->string('icon');
            $table->string('main_photo');
            $table->string('main_photo_thumbnail');
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('events');
	}

}