<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryPhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('gallery_photos', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->string('main_path');
            $table->string('thumbnail_path');
            $table->bigInteger('gallery_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('gallery_photos');
	}

}
