<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainPhotoPathToGallery extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('galleries', function($table) {
            $table->string('main_photo_path');
            $table->string('main_photo_thumbnail_path');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropColumn('galleries', function($table) {
            $table->dropColumn('main_photo_path');
            $table->dropColumn('main_photo_thumbnail_path');
        });
	}

}
