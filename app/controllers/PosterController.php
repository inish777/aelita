<?php

class PosterController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$start_date = date("Y-m-d", strtotime('this day'));
    $end_date = date("Y-m-d", strtotime('last day of this month'));
    $events = Event::whereBetween('date', [$start_date, $end_date])->orderBy('date', 'asc')->get();
    if(count($events) < 10) {
      $additional_events = Event::where('date', '>', $end_date)->take(10 - count($events))->orderBy('date', 'asc')->get();
      foreach($additional_events as $event) {
        $events->add($event);
      }
    }
    return View::make('site.index')->with('events', $events);
	}
}
