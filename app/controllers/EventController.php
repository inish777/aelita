<?php

class EventController extends \BaseController {
  
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$event = Event::find($id);
    if(!$event) App::abort(404);
    return View::make("event.show")->with("event", $event);
	}
}
