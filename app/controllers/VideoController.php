<?php

class VideoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$videos = VideoModel::paginate(12);
    $presenter = new \Presenters\VideoListPresenter($videos);
    $presenter->prepare();
    return View::make('video.index')->with('videos', $presenter);
	}
}
