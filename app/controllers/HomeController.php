<?php

class HomeController extends BaseController {

  /*
  |--------------------------------------------------------------------------
  | Default Home Controller
  |--------------------------------------------------------------------------
  |
  | You may wish to use controllers instead of, or in addition to, Closure
  | based routes. That's great! Here is an example controller method to
  | get you started. To route to this controller, just add the route:
  |
  | Route::get('/', 'HomeController@showWelcome');
  |
  */

  public function index()
  {
    $start_date = date("Y-m-d", strtotime('this day'));
    $end_date = date("Y-m-d", strtotime('last day of this month'));
    $events = Event::whereBetween('date', [$start_date, $end_date])->orderBy('date', 'asc')->get();
    
    return View::make('site.index')->with('events', $events);
  }

  public function login()
  {
    return View::make('site.login');
  }

  public function makelogin()
  {
    if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
      return Redirect::intended('dashboard');
    }
    else {
      return View::make("site.login")->with("error", "Ошибка входа: неверный логин или пароль");
    }
  }

  public function logout()
  {
    Auth::logout();
    return Redirect::to('/');
  }
}
