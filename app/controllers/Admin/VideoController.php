<?php 

namespace Admin;

use \View as View;
use \Response as Response;
use \Input as Input;
use \VideoModel as VideoModel;

class VideoController extends \BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $videos = VideoModel::paginate(12);
    return View::make('admin.video.index')->with('videos', $videos);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $video = new VideoModel;
    if($video->validate(['url' => Input::get('url')])) {
      try {
        $video->getIDFromURL(Input::get('url'));
      }
      catch(\Exception $e) {
        return View::make('admin.video.index')->with('videos', VideoModel::paginate(12))
          ->withErrors(['url' => ['Предоставленная ссылка не является ссылкой на видео']]);
      }
      $video->save();
      return View::make('admin.video.index')->with('videos', VideoModel::paginate(12));
    } else {
      Input::flash();
      return View::make('admin.video.index')->with('videos', VideoModel::paginate(12))->withErrors($video->validator);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    VideoModel::destroy($id);
    return Response::json(['status' => 'ok']);
  }
}
