<?php 

namespace Admin;

use \Banner;
use \View;
use \Input;
use \Redirect;
use \Response;

class BannerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$banners = Banner::all();
		return View::make('admin.banner.index')->with('banners', $banners);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.banner.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    $banner = new Banner;
    $input = Input::only("tooltip", "image", "url");
    if($banner->validate($input)) {
        $banner->tooltip = Input::get("tooltip");
        $banner->url = Input::get("url");
        $banner->saveImage(Input::file("image"));
        $banner->order = Banner::max('order') + 1;
        $banner->save();
        return Redirect::route('admin.banner.index');
    } else {
    		Input::flash();
        return View::make('admin.banner.create')->withErrors($banner->validator);
    }

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$banner = Banner::find($id);

		return View::make('admin.banner.edit')->with('banner', $banner);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::only("tooltip", "url", "order", "image");
    $banner = Banner::find($id);
    
    if($banner->validate($input)) {
        $banner->tooltip = Input::get("tooltip");
        $banner->url = Input::get("url");
        $banner->order = Input::get('order');
        if(Input::hasFile("image"))
            $banner->saveImage(Input::file("image"));
        $banner->save();
        
        return Redirect::route('admin.banner.index');
    } else {
        return View::make("admin.banner.edit")->with("banner", $banner)->withErrors($banner->validator);
    }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Banner::destroy($id);
    return Response::json(["status" => "ok"]);
	}
}
