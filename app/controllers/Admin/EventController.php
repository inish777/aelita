<?php

namespace Admin;

use \Event as Event;
use \View as View;
use \Input as Input;
use \Validator as Validator;

class EventController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $events = Event::orderBy('created_at', 'desc')->paginate(20);
        return View::make('admin.event.index')->with('events', $events);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.event.create');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::only("title", "description", "time", "date", "price");
        
        $event = new Event;
        
        //validate event
        if($event->validate($input)) {
            $event->date = Input::get("date");
            $event->title = Input::get("title");
            $event->description = Input::get("description");
            $event->price = Input::get("price");
            $event->time = Input::get("time");
            $event->type = Input::get("type");
            $image_rule = array("image" => "image");
            
            //validate and save small photo
            if(Input::hasFile("small-photo")) {
                $validator = Validator::make(array("image" => Input::file("small-photo")), $image_rule);
                if($validator->passes()) {
                    $event->saveSmallPhoto(Input::file("small-photo"));
                }
                else {
                    return View::make("admin.event.create")->withErrors($validator);
                }
            }
            
            //validate and save main-photo
            if(Input::hasFile("main-photo")) {
                $validator = Validator::make($image_rule, array("image" => Input::file("main-photo")));
                if($validator->passes()) {
                    $event->saveMainPhoto(Input::file("main-photo"));
                } else {
                    return View::make("admin.event.create")->withErrors($validator);
                }
            }

            $event->save();
            $events = Event::orderBy('created_at', 'desc')->paginate(20);
            return View::make('admin.event.index')->with("events", $events);
        }
        else {
            return View::make('admin.event.create')->withErrors($event->validator);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        return View::make("admin.event.edit")->with("event", $event);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {   
        $event = Event::find($id);
        if(!$event) App::Abort(404);
        
        $input = Input::only("title", "description", "time", "date", "price");
        //validate event
        if($event->validate($input)) {
            $event->date = Input::get("date");
            $event->title = Input::get("title");
            $event->description = Input::get("description");
            $event->price = Input::get("price");
            $event->time = Input::get("time");
            $event->type = Input::get("type");
            $image_rule = array("image" => "image");
            
            //validate and save small photo
            if(Input::hasFile("small-photo")) {
                $validator = Validator::make(array("image" => Input::file("small-photo")), $image_rule);
                if($validator->passes()) {
                    $event->saveSmallPhoto(Input::file("small-photo"));
                }
                else {
                    return View::make("admin.event.edit")->with("event", $event)->withErrors($validator);
                }
            }
            
            //validate and save main-photo
            if(Input::hasFile("main-photo")) {
                $validator = Validator::make($image_rule, array("image" => Input::file("main-photo")));
                if($validator->passes()) {
                    $event->saveMainPhoto(Input::file("main-photo"));
                } else {
                    return View::make("admin.event.edit")->with("event", $event)->withErrors($validator);
                }
            }

            $event->save();
            $events = Event::orderBy('created_at', 'desc')->paginate(20);
            return View::make('admin.event.index')->with("events", $events);
        }
        else {
            return View::make('admin.event.edit')->with("event", $event)->withErrors($event->validator);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Event::destroy($id);
        return \Response::json(array("status" => "ok"));
    }
}
