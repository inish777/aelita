<?php 

namespace Admin;

use \View as View;
use \Response as Response;
use \Input as Input;
use \Report as Report;
use \Redirect as Redirect;

class ReportController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$reports = Report::orderBy('created_at', 'desc')->get();
    
    return View::make('admin.report.index')->with('reports', $reports);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.report.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$report = new Report;
    
    $input = Input::only('title', 'image');
    
    if($report->validate($input)) {
      $report->title = $input['title'];
      if(Input::hasFile('image'))
        $report->saveImage(Input::file('image'));
      $report->save();
      return Redirect::route('admin.report.index');
    }
    
    Input::flash();
    return View::make('admin.report.create')->withErrors($report->validator);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Report::destroy($id);
    
    return Response::json(['status' => 'ok']);
	}
}
