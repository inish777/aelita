<?php 

namespace Admin;
use \Input as Input;
use \UserURL as UserURL;
use \Response as Response;
use \View as View;
use \Config as Config;
use \Redirect as Redirect;

class UrlController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$regions = Config::get('regions');
    $urls = $this->getURLs();
    
    return View::make('admin.url.index')->with('urls', $urls)->with('regions', $regions);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$url = new UserURL;
    $input = Input::only('url', 'url_text');
    if($url->validate($input)) {
      $url->url = Input::get('url');
      $url->url_text = Input::get('url_text');
      $url->region = Input::get('_region');
      $url->order = UserURL::where('region', Input::get('_region'))->max('order') + 1;
      $url->save();
    }
    else {
      return View::make('admin.url.index')
        ->with('regions', Config::get('regions'))
        ->with('urls', $this->getURLs())
        ->withErrors($url->validator);
    }
    return Redirect::route('admin.url.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    $url = UserURL::find($id);
    $input = Input::only('url', 'url_text', 'order');
    if($url->validate($input)) {
      $url->url = Input::get('url');
      $url->url_text = Input::get('url_text');
      $url->order = Input::get('order');
      $url->save();
    }
    else {
      return View::make('admin.url.index')
        ->with('regions', Config::get('regions'))
        ->with('urls', $this->getURLs())
        ->withErrors($url->validator);
    }
    return Redirect::route('admin.url.index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		UserURL::destroy($id);
    
    return Response::json(["status" => "ok"]);
	}
  
  private function getURLs()
  {
    $urls = [];
    $regions = Config::get('regions');
    foreach($regions as $region) {
      $urls[$region] = UserURL::where('region', $region)->orderBy('order', 'asc')->get();
    }
    
    return $urls;
  }
}
