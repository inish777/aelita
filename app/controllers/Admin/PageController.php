<?php

namespace Admin;

use \Page as Page;
use \View as View;
use \Input as Input;
use \Response as Response;

class PageController extends \BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $pages = Page::orderBy('created_at', 'desc')->paginate(10);
    return View::make("admin.page.index")->with("pages", $pages);
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return View::make("admin.page.create");
  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    $page = new Page;
    if($page->validate(['title' => Input::get('title')])) {
      $page->title = Input::get("title");
      $page->content = Input::get("content");
      $page->url = Input::get("url");
      $page->additional_html = Input::get('additional-html');
      $page->save();
      
      $pages = Page::orderBy('created_at', 'desc')->paginate(10);
      return View::make("admin.page.index")->with("pages", $pages);
    }
    
    $pages = Page::orderBy('created_at', 'desc')->paginate(10);
    Input::flash();
    return View::make("admin.page.create")->withErrors($page->validator);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $page = Page::find($id);
    return View::make("admin.page.edit")->with("page", $page);
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $page = Page::find($id);
    if($page->validate(['title' => Input::get('title')])) {
      $page->title = Input::get('title');
      $page->content = Input::get('content');
      $page->url = Input::get('url');
      $page->additional_html = Input::get('additional-html');
      $page->save();
      return View::make('admin.page.edit')->with('page', $page);
    }
    
    return View::make('admin.page.edit')->with('page', $page)->withErrors($page->validator);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    Page::destroy($id);
        
    return Response::json(['status' => 'ok']);
  }
}
