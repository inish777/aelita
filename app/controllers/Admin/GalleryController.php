<?php

namespace Admin;

use \View as View;
use \Response as Response;
use \Input as Input;
use \Gallery as Gallery;

class GalleryController extends \BaseController {
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $galleries = Gallery::all();
        return View::make('admin.gallery.index')->with("galleries", $galleries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::only("title", "description", "main_photo");
        $gallery = new Gallery;
        
        if($gallery->validate($input)) {
            $gallery->title = Input::get("title");
            $gallery->description = Input::get("description");
            if(Input::hasFile("main_photo"))
                $gallery->saveImage(Input::file("main_photo"));
            $gallery->save();
            
            return View::make("admin.gallery.edit")->with("gallery", $gallery);
        } else {
            Input::flash();
            return View::make("admin.gallery.create")->withErrors($gallery->validator);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $gallery = Gallery::find($id);
        if(!$gallery) App::abort(404);
        return View::make("admin.gallery.edit")->with("gallery", $gallery);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $gallery = Gallery::find($id);
        if(!$gallery) App::abort(404);
        
        $input = Input::only("title", "description", "main_photo");
        
        if($gallery->validate($input)) {
            $gallery->title = Input::get("title");
            $gallery->description = Input::get("description");
            if(Input::hasFile("main_photo"))
                $gallery->saveImage(Input::file("main_photo"));
            $gallery->save();
            return View::make("admin.gallery.edit")->with("gallery", $gallery);
        } else {
            Input::flash();
            return View::make("admin.gallery.edit")->with('gallery', $gallery)->withErrors($gallery->validator);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Gallery::destroy($id);
        return Response::json(["status" => "ok"]);
    }
}