<?php 

namespace Admin;

use \Gallery as Gallery;
use \GalleryPhoto as GalleryPhoto;
use \View as View;
use \Input as Input;
use \Response as Response;
use \Redirect as Redirect;

class GalleryPhotoController extends \BaseController {

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store($gallery)
  {
        $gallery_model = Gallery::find($gallery);
        if(!$gallery_model) App::abort(404);
        $photo = new GalleryPhoto;
        $input = Input::only("title", "photo");
        if($photo->validate($input)) {
            $photo->title = Input::get("title");
            $photo->saveImage(Input::file("photo"));
            $photo->gallery_id = $gallery;
            $photo->save();
            return Redirect::route('admin.gallery.photo.index', ['gallery' => $gallery]);
        } else {
            return View::make('admin.gallery.edit')->with('gallery', $gallery_model)->withErrors($photo->validator);
        }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($gallery, $photo)
  {
        GalleryPhoto::destroy($photo);
        return Response::json(["status" => "ok"]);
  }

    public function index($gallery)
    {
        $gallery = Gallery::find($gallery);
        if(!$gallery) App::abort(404);
        $photos = $gallery->photos;
        return View::make('admin.gallery.photo.index')->with("photos", $photos)->with('gallery', $gallery);
    }   
}
