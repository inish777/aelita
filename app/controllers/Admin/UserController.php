<?php 

namespace Admin;

use \User as User;
use \View as View;
use \Input as Input;

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = User::find(1);
    return View::make('admin.user.index')->with('user', $user);
	}

	public function update()
	{
		$user = User::find(1);
    if($user->validate(Input::only('login', 'password', 'confirmation'))) {
      $user->email = Input::get('login');
      if(Input::has('password'))
        $user->password = Hash::make(Input::get('password'));
      $user->save();
      return View::make('admin.user.index')->with('user', $user);
    }
    return View::make('admin.user.index')->with('user', $user)->withErrors($user->validator);
	}
}
