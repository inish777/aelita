<?php

class ArchiveController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$events = Event::where('date', '<', date('Y-m-d', strtotime("this day")))->orderBy('date', 'acs')->paginate(20);
    return View::make('archive.index')->with("events", $events);
	}
}
