<?php

class PageController extends \BaseController 
{

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $page = Page::where('url', $id)->first(); //search by slug first
    if(!$page) $page = Page::find($id); //then by id
    if(!$page) App::abort(404); //not found :-(
    return View::make("page.show")->with("page", $page);
  }
}
