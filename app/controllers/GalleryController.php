<?php

class GalleryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$galleries = Gallery::orderBy('created_at', 'desc')->paginate(18);
    $presenter = new Presenters\GalleryListPresenter($galleries);    
    $presenter->prepare();
        
    return View::make('gallery.index')->with('galleries', $presenter);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$gallery = Gallery::find($id);
		if(!$gallery) App::abort(404);
		$photos = new \Presenters\GalleryPhotoListPresenter($gallery->photos);
		$photos->prepare();
		return View::make('gallery.show')->with('photos', $photos)->with('gallery', $gallery);
	}
}
