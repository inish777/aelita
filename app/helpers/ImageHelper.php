<?php

namespace Helpers;

use \File as File;

class ImageHelper
{
  private $image;
  private $image_path;
  
  public __construct($image)
  {
    $this->image = $image;
  }
  
  public function addSubImage($name, $processor)
  {
    $image = InterventionImage::make($this->image_path);
    $processed_image = $processor($image);
    File::makeDirectory(public_path() . $ds . $main_dir . $ds . $name);
    
    $thumbnail_file_path = $main_dir . $ds . $name . $ds . $filename;
    $thumbnail->save(public_path() . $ds . $thumbnail_file_path);
    $this->thumbnail_path = $thumbnail_file_path;
  }
  
  public function save()
  {
    $name = $this->name;
    $ds = DIRECTORY_SEPARATOR;
    $photo_path = "uploads" . $ds . $name;
    if(!File::exists(public_path() . $ds . $photo_path))
        File::makeDirectory(public_path() . $ds . $photo_path);
    $uuid = uniqid();
    while(File::exists(public_path() . $ds . $photo_path . $ds . $uuid))
        $uuid = uniqid();
    $main_dir = $photo_path . $ds . $uuid;
    File::makeDirectory(public_path() . $ds . $main_dir);
    
    //save original photo
    File::makeDirectory(public_path() . $ds . $main_dir . $ds . "original");
    $filename = $image->getClientOriginalName();
    $file_path = $main_dir . $ds . "original" . $ds . $filename;
    $image->move(public_path() . $ds . $main_dir . $ds . "original",  $file_path);
    $this->image_path = public_path() . $ds . $file_path;
  }
}
