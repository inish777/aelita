<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('login', ['before' => 'guest', 'uses' => 'HomeController@login']);
Route::post('login', ['before' => 'guest', 'uses' => 'HomeController@makelogin']);
Route::get('logout', ['before' => 'auth', 'as' => 'logout', 'uses' => 'HomeController@logout']);
Route::get('reports', ['as' => 'reports', 'uses' => 'ReportController@index']);
Route::get('video', ['as' => 'video', 'uses' => 'VideoController@index']);
Route::get('poster', ['as' => 'poster', 'uses' => 'PosterController@index']);
Route::resource('event', 'EventController', ['only' => ['index', 'show']]);
Route::resource('page', 'PageController', ['only' => ['show']]);
Route::resource('gallery', 'GalleryController', ['only' => ['index', 'show']]);
Route::get('archive', ['as' => 'archive', 'uses' => 'ArchiveController@index']);

//routes for admin panel
Route::group(['before' => 'auth', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'EventController@index');
    Route::resource('event', 'EventController', ['except' => ['show']]);
    Route::resource('page', 'PageController', ['except' => ['show']]);
    Route::resource('gallery', 'GalleryController', ['except' => 'show']);
    Route::resource('gallery.photo', 'GalleryPhotoController', ['only' => ['store', 'destroy', 'index']]);
    Route::resource('video', 'VideoController', ['only' => ['index', 'store', 'destroy']]);
    Route::resource('report', 'ReportController');
    Route::resource('url', 'UrlController', ['except' => 'create']);
    Route::resource('banner', 'BannerController');
    Route::get('user', ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
    Route::post('user', ['as' => 'admin.user.update', 'uses' => 'UserController@update']);
});

//routes for elfinder
Route::group(array('before' => 'auth'), function() {
  Route::get('elfinder', 'Barryvdh\Elfinder\ElfinderController@showIndex');
  Route::any('elfinder/connector', 'Barryvdh\Elfinder\ElfinderController@showConnector');
  Route::get('elfinder/ckeditor4', 'Barryvdh\Elfinder\ElfinderController@showCKeditor4');
});  