@extends('layout.main')

@section('title')
  Зрелищный центр Аэлита &mdash; Архив
@stop

@section('content')
  @foreach($events as $index => $event)
    <div class="event {{{ $index == 0 || $index % 2 == 0 ? "event-odd" : "event-even" }}}">
      <div class="event-date">
        <div class="event-day">
          {{{ $event->getDay() }}}
        </div>
        <div class="event-month">
          {{{ $event->getMonth() }}}
        </div>
      </div>
      <div class="event-image">
        <img src="{{{ $event->icon }}}">
        <div class="event-type">
          {{{ $event->type }}}
        </div>
      </div>
      <div class="event-middle-column">
        <div class="event-title">
          <a href="{{{ route("event.show", array("event" => $event->id))  }}}">{{{ $event->title }}}</a>
        </div>
        <div class="event-description">
          {{ str_limit($event->getDescriptionAsText(), 110, '...')  }}
        </div>
      </div>
      <div class="event-right-column">
        <div class="event-time"><img src="{{{ URL::to("images/clock-icon.png") }}}" style="vertical-align: sub;"> {{{ $event->time }}}</div>
        <div class="event-price"><img src="{{{ URL::to("images/rouble-icon.png") }}}" style="vertical-align: sub;"> {{{ $event->price }}}</div>
        <div class="event-details">
	      <a class="event-details-href" href="{{{ route("event.show", array("event" => $event->id)) }}}"> &rsaquo;&rsaquo; Подробнее </a> 
        </div>
      </div>
    </div>
  @endforeach
  {{ $events->links() }}
@stop
