@extends('layout.main')

@section('content')
  <h1 style="text-align: center;">404</h1>
  <h2 style="text-align: center;">Страница не найдена</h2>
  <img src="{{{ URL::to('images/404.jpg') }}}" style="margin: 20px 0 0 60px;">
@stop
