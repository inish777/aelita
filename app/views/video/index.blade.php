@extends('layout.main')

@section('content')
  @foreach($videos->getRows() as $row_num => $row)
    <div class="{{{ $videos->getRowClasses($row_num) }}}">
      @foreach($row['videos'] as $video)
        <div class="{{{ $video->getClasses() }}}">
          <iframe width="340" height="255" src="//www.youtube.com/embed/{{{$video->getId()}}}" frameborder="0" allowfullscreen></iframe>
        </div>
      @endforeach
    </div>
  @endforeach
  {{ $videos->getPager()->links() }}
@stop
