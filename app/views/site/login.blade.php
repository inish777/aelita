@extends('layout.login')

@section('content')
  @if(!empty($error))
    <div class="alert alert-danger">{{{ $error }}} </div>
  @endif
  <legend><h1>Логин</h1></legend>
  <form class="form-horizontal" method="POST" action="{{{ URL::to("login") }}}">
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Логин</label>
      </div>
      <div class="col-lg-2">
        <input type="text" class="form-control" name="email">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Пароль</label>
      </div>
      <div class="col-lg-2">
        <input type="password" class="form-control" name="password">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3 col-lg-offset-3">
        <button type="submit" class="btn btn-default">Вход</button>
      </div>
    </div>
  </form>
@stop
