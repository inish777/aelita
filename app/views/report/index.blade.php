@extends('layout.main')

@section('content')
  @foreach($reports->getRows() as $row_num => $row)
    <div class="{{{ $reports->getRowClasses($row_num) }}}">
      @foreach($row['reports'] as $report)
        <div class="{{{ $report->getClasses() }}}">
          <div class="report-image">
            <a data-title="{{{ $report->getTitle() }}}" class="report-image-link" href="{{{ $report->getURLToImage() }}}" rel="report-photo">
              <img src="{{{ $report->getURLToThumbnail() }}}">
            </a>
          </div>
          <div class="report-title">
            {{{ $report->getTitle() }}}
          </div>
        </div>
      @endforeach
    </div>
  @endforeach
  <div id="hypercomments_widget"></div>
  <script type="text/javascript">
    _hcwp = window._hcwp || [];
    _hcwp.push({widget:"Stream", widget_id: 21882});
    (function() {
      if("HC_LOAD_INIT" in window)return;
      HC_LOAD_INIT = true;
      var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
      var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
      hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/21882/"+lang+"/widget.js";
      var s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(hcc, s.nextSibling);
    })();
  </script>
  <a href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>   
@stop
