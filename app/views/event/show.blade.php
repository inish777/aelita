@extends('layout.main')
    
@section('content')
  <div class="event-inner-header">
    <div class="event-date">
      <div class="event-day">
        {{{ $event->getDay() }}}
      </div>
      <div class="event-month">
        {{{ $event->getMonth() }}}
      </div>
    </div>
    <div class="event-inner-header-right-column">
      <div class="event-inner-title">
        {{{ $event->title }}}
      </div>
      <div class="event-inner-time-and-price">
        <div class="event-inner-time">
          <img style="vertical-align: middle;" src="{{{ URL::to("images/clock-icon-big.png") }}}"> 
          <span style="vertical-align: middle;"> {{{ $event->time }}} </span>
        </div>
        <div class="event-inner-price">
          <img style="vertical-align: middle;" src="{{{ URL::to("images/rouble-icon-big.png") }}}"> 
          <span style="vertical-align: middle;"> {{{ $event->price }}} </span>
        </div>
      </div>
    </div>
  </div>
  @if($event->main_photo)
    <div class="event-inner-main-photo">
      <img src="{{{ URL::to($event->main_photo_thumbnail) }}}">
    </div>
  @endif
  <div class="event-inner-description">
    {{ $event->description }}
  </div>
@stop
