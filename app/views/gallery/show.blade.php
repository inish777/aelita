@extends('layout.main')

@section('content')
  <h1 class="gallery-photos-header">{{{ $gallery->title }}}</h1>
  <div class="gallery-photos-description">
    {{ $gallery->description }}
  </div>
  @foreach($photos->getRows() as $row_num => $row)
    <div class="{{{ $photos->getRowClasses($row_num) }}}">
      @foreach($row['photos'] as $photo)
        <div class="{{{ $photo->getClasses() }}}">
          <a data-title="{{{ $photo->getTitle() }}}" rel='gallery-photo' class="gallery-photo-link" href="{{{ $photo->getURLToPhoto() }}}">
            <img src="{{{ $photo->getURLToPhotoThumbnail() }}}">
          </a>
        </div>
      @endforeach
    </div>
  @endforeach
@stop
