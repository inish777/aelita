@extends('layout.main')

@section('title')
  Зрелищный центр Аэлита - Галлерея
@stop

@section('content')
  @foreach($galleries->getRows() as $row_num => $row)
    <div class="{{{ $galleries->getRowClasses($row_num) }}}">
      @foreach($row["galleries"] as $gallery)
        <div class="{{{ $gallery->getClasses() }}}">
          <a class="gallery-title" href="{{{ route('gallery.show', ['gallery' => $gallery->getGalleryId()]) }}}">{{{ $gallery->getTitle() }}}</a>
          <div class="gallery-image">
            <img src="{{{ URL::to($gallery->getURLToMainPhotoThumbnail()) }}}">
          </div>
          <div class="gallery-watch-button">
            <a class="gallery-watch-button-link" href="{{{ route('gallery.show', ['gallery' => $gallery->getGalleryId()]) }}}">Смотреть &gt;&gt;</a>
          </div>
        </div>
      @endforeach
    </div>
  @endforeach  
@stop
