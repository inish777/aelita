@extends('layout.main')

@section('title')
  {{{ $page->title }}}
@stop

@section('content')
  <div style="margin-bottom: 20px;">
    <a href="{{{ URL::to('/') }}}">Главная</a> >> <a href="{{{ route('page.show', array('page' => $page->id)) }}}">{{{ $page->title }}}</a>
  </div>
  {{ $page->content }}
  {{ $page->additional_html }}
@stop
