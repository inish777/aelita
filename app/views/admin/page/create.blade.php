@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Добавить страницу</h2></legend>
  <form class="form-horizontal" action="{{{ route("admin.page.store") }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Название</label>
      </div>
      <div class="col-lg-4">
        <input type="text" class="form-control" name="title" value="{{{ Input::old('title') }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Адрес страницы на латинице</label>
      </div>
      <div class="col-lg-4">
        <input type="text" class="form-control" name="url" value="{{{ Input::old('url') }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Содержание
        </label>
      </div>
      <div class="col-lg-9">
        <textarea id="add-page-textarea" name="content">{{ Input::old('content') }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Дополнительный HTML-код
        </label>
      </div>
      <div class="col-lg-9">
        <textarea name="additional-html" class="form-control">{{ Input::old('additional-html') }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-4 col-lg-offset-3">
        <button class="btn btn-default" type="submit">Создать</button>
      </div>
    </div>
  </form>
@stop
