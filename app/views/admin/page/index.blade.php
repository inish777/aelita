@extends('layout.admin')

@section('content')
  <a href="{{{ route('admin.page.create') }}}" class="btn btn-default">Создать страницу</a>
  <table class="table" style="margin-top: 10px;">
    <tr>
      <th>Заголовок</th>
      <th>Ссылка</th>
      <th>Дата создания</th>
      <th>Действия</th>
    </tr>
    @foreach ($pages as $page)
      <tr class="page" data-id="{{{ $page->id }}}">
        <td> {{{ $page->title }}} </td>
        <td> {{{ route('page.show', ['page' => $page->getIDForUrl()]) }}} </td>
        <td> {{{ $page->created_at->format('d.m.Y H:i:s') }}} </td>
        <td>
          <a class="btn btn-default" href="{{{ route('admin.page.edit', ['page' => $page->id]) }}}">Редактировать</a>
          <a class="btn btn-danger delete-page-button" href="{{{ route('admin.page.destroy', ['page' => $page->id]) }}}" data-id="{{{ $page->id }}}">Удалить</a>
        </td>
      </tr>
    @endforeach
  </table>
  {{ $pages->links() }}
@stop
