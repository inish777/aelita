@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Редактировать страницу</h2></legend>
  <form class="form-horizontal" action="{{{ route("admin.page.update", array("page" => $page->id)) }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Название</label>
      </div>
      <div class="col-lg-4">
        <input type="text" class="form-control" name="title" value="{{{ $page->title }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Адрес страницы на латинице</label>
      </div>
      <div class="col-lg-4">
        <input type="text" class="form-control" name="url" value="{{{ $page->url }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Содержание
        </label>
      </div>
      <div class="col-lg-9">
        <textarea id="add-page-textarea" name="content">{{ $page->content }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Дополнительный HTML-код
        </label>
      </div>
      <div class="col-lg-9">
        <textarea name="additional-html" class="form-control">{{ $page->additional_html }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-4 col-lg-offset-3">
        <button class="btn btn-default" type="submit">Сохранить</button>
      </div>
    </div>
    <input type="hidden" name="_method" value="put">
  </form>
@stop
