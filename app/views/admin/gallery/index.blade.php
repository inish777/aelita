@extends('layout.admin')

@section('content')
  <a href="{{{ route("admin.gallery.create") }}}" class="btn btn-default">Создать галлерею</a>
  <table class="table" style="margin-top: 10px;">
    <tr>
      <th>Название</th>
      <th>Главное фото</th>
      <th>Время создания</th>
      <th>Действия</th>
    </tr>
    @foreach($galleries as $gallery)
      <tr class="gallery" data-id="{{{ $gallery->id }}}">
        <td>{{{ $gallery->title }}}</td>
        <td><img src="{{{ URL::to($gallery->main_photo_thumbnail_path) }}}"></td>
        <td>{{{ $gallery->created_at }}}</td>
        <td>
          <a class="btn btn-default" href="{{{ route('admin.gallery.edit', ["gallery" => $gallery->id]) }}}">Редактировать</a>
          <a data-id="{{{ $gallery->id }}}" class="btn btn-danger delete-gallery-button" href="{{{ route('admin.gallery.destroy', ['gallery' => $gallery->id]) }}}">Удалить</a>
          <a class="btn btn-default" href="{{{ route('admin.gallery.photo.index', ["gallery" => $gallery->id]) }}}">Добавить или удалить фото</a>
        </td>
      </tr>
    @endforeach
  </table>
@stop
