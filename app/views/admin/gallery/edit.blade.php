@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <a href="{{{ route('admin.gallery.photo.index', ['gallery' => $gallery->id]) }}}">Добавить или удалить фотографии</a>
  <legend>
    <h2>Редактировать галлерею</h2> 
  </legend>
  
  <form class="form-horizontal" action="{{{ route('admin.gallery.update', ['gallery' => $gallery->id]) }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-2">
        <label>Название</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="title" class="form-control" value="{{{ $gallery->title }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label>Описание</label>
      </div>
      <div class="col-lg-10">
        <textarea id="gallery-description-textarea" name="description">{{ $gallery->description }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label>Главное фото</label>
      </div>
      <div class="col-lg-3">
        <input type="file" name="main_photo">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2 col-lg-offset-2">
        <button type="submit" class="btn btn-default">Сохранить</button>
      </div>
    </div>
    <input type="hidden" name="_method" value="put">
  </form>
@stop
