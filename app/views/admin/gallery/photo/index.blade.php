@extends('layout.admin')

@section('content')
  
  <table class="table">
    <tr>
      <th>Заголовок</th>
      <th>Изображение</th>
      <th>Действия</th>
    </tr>
    @foreach($photos as $photo)
      <tr class="gallery-photo" data-id="{{{ $photo->id }}}">
        <td>{{{ $photo->title }}}</td>
        <td><img src="{{{ URL::to($photo->thumbnail_path) }}}"></td>
        <td>
          <a 
            href="{{{ route('admin.gallery.photo.destroy', ['gallery' => $gallery->id, 'photo' => $photo->id]) }}}" 
            class="btn btn-danger gallery-photo-delete-button" 
            data-id="{{{ $photo->id }}}">
            Удалить
          </a>
        </td>
      </tr>
    @endforeach
  </table>
  <legend><h2>Добавить фото в галлерею</h2></legend>
  <form class="form-horizontal" action="{{{ route('admin.gallery.photo.store', ['gallery' => $gallery->id]) }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-2">
        <label>Название</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="title" class="form-control" value="{{{ Input::old('title') }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label>Фото</label>
      </div>
      <div class="col-lg-3">
        <input type="file" name="photo">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2 col-lg-offset-2">
        <button type="submit" class="btn btn-default">Сохранить</button>
      </div>
    </div>
  </form>
@stop
