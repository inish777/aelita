@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Создать галлерею</h2></legend>
  <form class="form-horizontal" action="{{{ route('admin.gallery.store') }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Название</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="title" class="form-control" value="{{{ Input::old('title') }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Описание</label>
      </div>
      <div class="col-lg-10">
        <textarea id="gallery-description-textarea" name="description">{{ Input::old('description') }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Главное фото</label>
      </div>
      <div class="col-lg-3">
        <input type="file" name="main_photo">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2 col-lg-offset-2">
        <button type="submit" class="btn btn-default">Сохранить</button>
      </div>
    </div>
  </form>
@stop
