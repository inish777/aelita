@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Редактирование пользователя</h2></legend>
  <form class="form-horizontal" action="{{{ route('admin.user.update') }}}" method="POST">
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Логин</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="login" class="form-control" value="{{{ $user->email }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Новый пароль</label>
      </div>
      <div class="col-lg-3">
        <input type="password" name="password" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Еще раз</label>
      </div>
      <div class="col-lg-3">
        <input type="password" name="confirmation" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2 col-lg-offset-2">
        <button type="submit" class="btn btn-default">Сохранить</button>
      </div>
    </div>
  </form>
@stop
