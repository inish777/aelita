@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  @foreach($regions as $region)
    <legend><h2>{{{ Lang::get('regions.' . $region) }}}</h2></legend>
    <table class="table">
      <tr>
        <th>Текст</th>
        <th>URL</th>
        <th>Дата создания</th>
        <th>Порядок</th>
        <th>Действия</th>
      </tr>
      @foreach($urls[$region] as $url)
        <tr class="url" data-id="{{{ $url->id }}}">
          <td>{{{ $url->url_text }}}</td>
          <td>{{{ $url->url }}}</td>
          <td>{{{ $url->created_at }}}</td>
          <td>{{{ $url->order }}}</td>
          <td>
            <button data-toggle="modal" data-target="#url-popup-{{{$url->id}}}" class="btn btn-default">
              Редактировать
            </button>
            <a href="{{{ route('admin.url.destroy', ['url' => $url->id]) }}}" data-id="{{{ $url->id }}}" class="btn btn-danger delete-url-button">
              Удалить
            </a>
          </td>
        </tr>
      @endforeach
    </table>
    <form class="form-horizontal" action="{{{ route('admin.url.store') }}}" method="POST">
      <div class="form-group">
        <div class="col-lg-1">
          <label class="control-label">URL</label>
        </div>
        <div class="col-lg-2">
          <input type="text" name="url" class="form-control">
        </div>
        <div class="col-lg-1">
          <label class="control-label">Текст</label>
        </div>
        <div class="col-lg-2">
          <input type="text" name="url_text" class="form-control">
        </div>
        <div class="col-lg-2">
          <button type="submit" class="btn btn-default">Добавить</button>
        </div>
      </div>
      <input type="hidden" name="_region" value="{{{ $region }}}">
    </form>
  @endforeach
  
  @foreach($regions as $region)
    @foreach($urls[$region] as $url)
      <div id="url-popup-{{{$url->id}}}" class="modal fade" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <form class="form-horizontal" action="{{{ route('admin.url.update', ['url' => $url->id]) }}}" method="POST">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                  <h4 class="modal-title">Редактирование ссылки</h4>
                </div>
                <div class="modal-body">
                  <label class="control-label">URL</label>
                  <input type="text" name="url" class="form-control" value="{{{ $url->url }}}">
                  <label class="control-label">Текст</label>
                  <input type="text" name="url_text" class="form-control" value="{{{ $url->url_text }}}">
                  <label class="control-label">Порядок</label>
                  <input type="text" name="order" class="form-control" value="{{{ $url->order }}}">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                  <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
                <input type="hidden" name="_method" value="put">
              </form>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    @endforeach
  @endforeach
@stop
