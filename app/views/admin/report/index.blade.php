@extends('layout.admin')

@section('content')
  <a href="{{{ route('admin.report.create') }}}" class="btn btn-default">
    Создать отзыв
  </a>
  <table class="table" style="margin-top: 10px;">
    <tr>
      <th>Название</th>
      <th>Изображение</th>
      <th>Дата создания</th>
      <th>Действия</th>
    </tr>
    @foreach($reports as $report)
      <tr class="report" data-id="{{{ $report->id}}}">
        <td>{{{ $report->title }}}</td>
        <td><img src="{{{ $report->getURLToThumbnail() }}}"></td>
        <td>{{{ $report->created_at }}}</td>
        <td>
          <a href="{{{ route('admin.report.destroy', ['report' => $report->id]) }}}" class="btn btn-danger remove-report-button" data-id="{{{ $report->id }}}">
            Удалить
          </a>
        </td>
      </tr>
    @endforeach
  </table>
@stop
