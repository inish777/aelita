@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Создать отзыв</h2></legend>
  <form class="form-horizontal" action="{{{ route('admin.report.store') }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Название</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="title" class="form-control" value="{{{ Input::old('title') }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        <label class="control-label">Изображение</label>
      </div>
      <div class="col-lg-3">
        <input type="file" name="image">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2 col-lg-offset-2">
        <button type="submit" class="btn btn-default">Сохранить</button>
      </div>
    </div>
  </form>
@stop
