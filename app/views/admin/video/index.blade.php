@extends('layout.admin')

@section('content')
  <form class="form-horizontal" method="POST" action="{{{ route('admin.video.store') }}}">
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Ссылка</label>
      </div>
      <div class="col-lg-4">
        <input type="text" class="form-control" name="url">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-offset-3 col-lg-2">
        <button type="submit" class="btn btn-default">Добавить</label>
      </div>
    </div>
  </form>
  
  <table class="table">
    <tr>
      <th>Видео</th>
      <th>Дата добавления</th>
      <th>Действия</th>
    </tr>
    @foreach($videos as $video)
      <tr class="video" data-id="{{{ $video->id }}}">
        <td><iframe width="340" height="255" src="//www.youtube.com/embed/{{{$video->video_id}}}" frameborder="0" allowfullscreen></iframe></td>
        <td>{{{ $video->created_at }}}</td>
        <td>
          <a href="{{{ route('admin.video.destroy', ['video' => $video->id]) }}}" class="btn btn-danger video-delete-button" data-id="{{{$video->id}}}">Удалить</a>
        </td>
      </tr>
    @endforeach
  </table>
  {{ $videos->links() }}
@stop
