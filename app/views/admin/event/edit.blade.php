@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Редактировать событие</h2></legend>
  <form class="form-horizontal" action="{{{ route("admin.event.update", array("event" => $event->id)) }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Название</label>
      </div>
      <div class="col-lg-4">
        <input type="text" class="form-control" name="title" value="{{{ $event->title }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Дата</label>
      </div>
      <div class="col-lg-2">
        <input type="text" class="form-control" name="date" id="add-event-date-field" value="{{{ date("d.m.Y", strtotime($event->date)) }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Цена</label>
      </div>
      <div class="col-lg-2">
        <input type="text" class="form-control" name="price" value="{{{ $event->price }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Время</label>
      </div>
      <div class="col-lg-2">
        <input type="text" class="form-control" name="time" value="{{{ $event->time  }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Описание
        </label>
      </div>
      <div class="col-lg-9">
        <textarea id="add-event-textarea" name="description">{{ $event->description }}</textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Тип события
        </label>
      </div>
      <div class="col-lg-2">
        <input type="text" name="type" class="form-control" value="{{{ $event->type }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Фото 80x80 в заголовке
        </label>
      </div>
      <div class="col-lg-4">
        <input type="file" name="small-photo">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">
          Главное фото
        </label>
      </div>
      <div class="col-lg-4">
        <input type="file" name="main-photo">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-4 col-lg-offset-3">
        <button class="btn btn-default" type="submit">Сохранить</button>
      </div>
    </div>
    <input type="hidden" name="_method" value="put">
  </form>
@stop
