@extends('layout.admin')

@section('content')
  <div class="row">
    <div class="col-lg-2">
      <a class="btn btn-default" href="{{ route("admin.event.create")  }}">Добавить событие</a>
    </div>
  </div>
  <table class="table" style="margin-top: 10px;">
    <tr>
      <th>Дата</th>
      <th>Заголовок</th>
      <th>Дата создания</th>
      <th> Действия </th>
    </tr>
    @foreach($events as $event)
      <tr class="event" data-id="{{{ $event->id }}}">
        <td>{{{  $event->date->format("d.m.Y") }}} </td>
        <td>{{{ $event->title }}} </td>
        <td>{{{ $event->created_at->format("d.m.Y H:i:s")  }}}</td>
        <td>
          <a class="btn btn-default" href="{{{ route("admin.event.edit", array("event" => $event->id)) }}}">Редактировать</a>
          <a class="btn btn-danger delete-event-button" href="{{{ route("admin.event.destroy", array("event" => $event->id)) }}}" data-id="{{{ $event->id }}}">Удалить</a>
        </td>
      </tr>
    @endforeach
  </table>
  {{ $events->links() }}
@stop
