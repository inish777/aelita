@extends('layout.admin')

@section('content')
  @foreach($errors->all() as $error)
    <div class="alert-danger alert">{{ $error }}</div>
  @endforeach
  <legend><h2>Редактировать баннер</h2></legend>
  <form class="form-horizontal" action="{{{ route('admin.banner.update', ['banner' => $banner->id]) }}}" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Порядок</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="order" class="form-control" value="{{{ $banner->order }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Всплывающая подсказка при наведении</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="tooltip" class="form-control" value="{{{ $banner->tooltip }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Ссылка</label>
      </div>
      <div class="col-lg-3">
        <input type="text" name="url" class="form-control" value="{{{ $banner->url }}}">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-3">
        <label class="control-label">Изображение</label>
      </div>
      <div class="col-lg-2">
        <input type="file" name="image">
      </div>
    </div>
    <input type="hidden" name="_method" value="put">
    <div class="form-group">
      <div class="col-lg-2 col-lg-offset-1">
        <button type="submit" class="btn btn-default">Сохранить</button>
      </div>
    </div>
  </form>
@stop