@extends('layout.admin')

@section('content')
  <div class="row">
    <div class="col-lg-2">
      <a class="btn btn-default" href="{{ route("admin.banner.create")  }}">Добавить баннер</a>
    </div>
  </div>
  <table class="table" style="margin-top: 10px;">
    <tr>
      <th>Порядок</th>
      <th>Изображение</th>
      <th>Всплывающая подсказка</th>
      <th>Ссылка</th>
      <th> Действия </th>
    </tr>
    @foreach($banners as $banner)
      <tr class="banner" data-id="{{{ $banner->id }}}">
        <td>{{{ $banner->order }}}</td>
        <td><img src="{{{ URL::to($banner->image) }}}"></td>
        <td>{{{ $banner->tooltip }}} </td>
        <td>{{{ $banner->url }}}</td>
        <td>
          <a class="btn btn-default" href="{{{ route('admin.banner.edit', ['banner' => $banner->id]) }}}">Редактировать</a>
          <a class="btn btn-danger delete-banner-button" href="{{{ route('admin.banner.destroy', ['banner' => $banner->id]) }}}" data-id="{{{ $banner->id }}}">Удалить</a>
        </td>
      </tr>
    @endforeach
  </table>
@stop