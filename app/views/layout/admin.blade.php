<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<link rel="stylesheet" type="text/css" href="{{{ URL::to('/css/bootstrap.min.css') }}}" />
  <link rel="stylesheet" type="text/css" href="{{{ URL::to('/css/bootstrap-theme.css') }}}" />
  <link rel="stylesheet" href=" {{{ URL::to('css/jquery-ui.min.css') }}}">

  <script type="text/javascript" src="{{{ URL::to('js/jquery-2.1.1.min.js') }}}"></script>
  <script type="text/javascript" src="{{{ URL::to('ckeditor/ckeditor.js') }}}"></script>
  <script type="text/javascript" src="{{{ URL::to('ckeditor/adapters/jquery.js') }}}"></script>
  <script type="text/javascript" src="{{{  URL::to('js/bootstrap.min.js') }}}"></script>
  <script src="{{{ URL::to('js/jquery-ui.min.js') }}}"></script>
  <script src="{{{ URL::to('js/jquery.iframe-transport.js') }}}"></script>
  <script type="text/javascript" src="{{{ URL::to('js/admin.js') }}}"></script>
	<title>
    @section('title') 
      Зрелищный центр Аэлита - Админпанель 
    @show
  </title>
</head>

<body>
  <header>
  </header>
  <nav role="navigation" id="nav-menu" class="navbar navbar-default">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{{ URL::to('/') }}}">Аэлита</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="{{{ Request::is('admin', 'admin/event') ? 'active' : '' }}}"><a href="{{{ URL::to('admin/') }}}">События</a></li>
        <li class="{{{ Request::is('admin/page') ? 'active' : '' }}}"><a href="{{{ route('admin.page.index') }}}">Страницы</a></li>
        <li class="{{{ Request::is('admin/gallery') ? 'active' : '' }}}"><a href="{{{ route('admin.gallery.index') }}}">Галлереи</a></li>
        <li class="{{{ Request::is('admin/video') ? 'active' : '' }}}"><a href="{{{ route('admin.video.index') }}}">Видео</a></li>
        <li class="{{{ Request::is('admin/user') ? 'active' : '' }}}"><a href="{{{ route('admin.user.index') }}}">Пользователь</a></li>
        <li class="{{{ Request::is('admin/report') ? 'active' : ''}}}"><a href="{{{ route('admin.report.index') }}}">Отзывы</a></li>
        <li class="{{{ Request::is('admin/url') ? 'active' : ''}}}"><a href="{{{ route('admin.url.index') }}}">Ссылки</a></li>
        <li class="{{{ Request::is('admin/banner') ? 'active' : '' }}}"><a href="{{{ route('admin.banner.index') }}}">Баннеры</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{{ route('logout') }}}">Выход</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">
    @section('content')
    @show
  </div>
</body>
</html>
