<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<link rel="stylesheet" type="text/css" href="{{{ URL::to("/css/bootstrap.min.css") }}}" />
    <link rel="stylesheet" type="text/css" href="{{{ URL::to("/css/bootstrap-theme.css") }}}" />
    <link rel="stylesheet" href=" {{{ URL::to("css/jquery-ui.min.css") }}}">

    <script type="text/javascript" src="{{{ URL::to("js/jquery-2.1.1.min.js") }}}"></script>
    <script type="text/javascript" src="{{{  URL::to("js/bootstrap.min.js") }}}"></script>
    <script src="{{{ URL::to("js/jquery-ui.min.js") }}}"></script>
    <title>
      @section('title') 
        Зрелищный центр Аэлита 
      @show
    </title>
</head>

<body>
  <div class="container">
    @section('content')
    @show
  </div>
</body>
</html>
