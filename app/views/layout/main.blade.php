<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="ru" />

    <link rel="stylesheet" type="text/css" href="{{{ URL::to('/css/main.css') }}}" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link href="{{{ URL::to('css/colorbox.css') }}}" rel="stylesheet" type="text/css"></link>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="{{{ URL::to('js/jquery-2.1.1.min.js') }}}" type="text/javascript"></script>
    <script src="{{{ URL::to('js/jquery-ui.min.js') }}}" type="text/javascript"></script>
    <script src="{{{ URL::to('js/jquery.colorbox-min.js') }}}" type="text/javascript"></script>
    <script src="{{{ URL::to('js/main.js') }}}" type="text/javascript"></script>
    <link href="http://hiwidget.ru/public/css/spl.css" rel="stylesheet">
    <script src="http://hiwidget.ru/public/js/spl.js"></script>
    <script>
      var spl = new Spl(11, location.href);
      spl.start();
    </script>
    <title>
      @section('title')
        Аэлита - зрелищный центр
      @show
    </title>
    
    @section('links')
    @show
  </head>

  <body>
    <header>
      <div id="header-content">
        <a href="{{{ URL::to('/') }}}"><img id="site-logo" src="{{{ URL::to('/images/logo.png') }}}"></a>
        <img id="traces" src="{{{ URL::to('/images/traces.png') }}}" style="position: relative; top: 20px; left: 28px;">
        <div style="height: 40px; background: #23262b; width: 250px; font-size: 10px; border-radius: 5px; line-height: 40px; text-align: center; float: right; color: #fff;">
          Телефоны для справок и заказа билетов:
        </div>
        <span style = "float: right; position: relative; color: rgb(255, 255, 255); top: -29px;"><img src="{{{ URL::to('images/mobile.png') }}}"> 8(3822) 51-44-36 | 8(3822) 51-61-31</span>
        <span style="float: right; font-size: 12px; color: rgb(255, 255, 255); right: -275px; position: relative; top: 0px;"><img src="{{{ URL::to('images/location.png') }}}"> г. Томск, пр. Ленина, 78 (ост. ТЮЗ)</span>
      </div>
    </header>
    <nav role="navigation" id="nav-menu">
      <ul class="navigation">
        <li>
          <a class="{{{ Request::is('/') ? 'active' : '' }}}" href="{{{ URL::to('/') }}}">Главная</a>
        </li>
        @foreach($urls["menu"] as $url)
          <li>
            <a href="{{{ $url->url }}}" class="{{{ Request::url() == URL::to($url->url) ? 'active' : '' }}}"> {{{ $url->url_text }}} </a>
          </li>
        @endforeach
      </ul>
    </nav>
    <div id="main-page-wrapper">
      <div id="content">
        @section('content')
          
        @show
      </div>
      <div id="right-column">
        <a href="{{{ route('page.show', array('page' => 9)) }}}"><img src="{{{ Url::to('images/projects/new-water.png') }}}"></a>
        <a href="{{{ route('page.show', array('page' => 13)) }}}"><img src="{{{ Url::to('images/projects/children.png') }}}"></a>
        <a href="{{{ route('page.show', array('page' => 12)) }}}"><img src="{{{ Url::to('images/projects/cinema-club.png') }}}"></a>
        <a href="{{{ route('page.show', array('page' => 10)) }}}"><img src="{{{ Url::to('images/projects/live-music.png') }}}"></a>
        <a href="{{{ route('page.show', array('page' => 8)) }}}"><img src="{{{ Url::to('images/projects/theatre.png') }}}"></a>
        <a href="{{{ route('page.show', array('page' => 11)) }}}"><img src="{{{ Url::to('images/projects/our-partners.png') }}}"></a>
        @foreach($banners as $banner)
          <a target="_blank" href="{{{ $banner->url }}}" title="{{{ $banner->tooltip }}}"><img src="{{{ URL::to($banner->image) }}}"></a>
        @endforeach
      </div>
    </div>
    <footer>
      <div id="footer-content">
        <div id="to-guests">
          ЗРИТЕЛЯМ:
          <ul>
            @foreach($urls["to-visitors"] as $url)
              <li><a href="{{{ $url->url  }}}">{{{ $url->url_text }}}</a></li>
            @endforeach
          </ul>
        </div>
        <div id="about-us">
          О ЦЕНТРЕ:
          <ul>
            @foreach($urls["about-us"] as $url)
              <li><a href="{{{ $url->url }}}">{{{ $url->url_text }}}</a></li>
            @endforeach
          </ul>
        </div>
        <div id="timetable">
          РАСПИСАНИЕ РАБОТЫ КАССЫ:
          <p>пн: выходной</p>
          <p>вт-сб: с 13-00 до 20-00</p>
          <p><span style="color: #f39c12;">вс</span>: с 11-00 до 19-00</p>
          <p>перерыв: с 15-00 до 16-00</p>
        </div>
        <div id="contacts">
          КОНТАКТЫ:
          <p><a href="http://aelita.tom.ru" target="_blank">www.aelita.tom.ru</a></p>
          <p>Email: <a href="mailto: aelita.press@gmail.com"> aelita.press@gmail.com</a></p>
          <p>Адрес: 634050, Россия, г.Томск, пр.Ленина, 78 (ост. ТЮЗ)</p>
          <div id="social-buttons">
            <a href="http://vk.com/aelita.tomsk"><img src="{{{ URL::to('images/vk.png') }}}"></a>
            <a href="https://www.facebook.com/aelita.tomsk"><img src="{{{ URL::to('images/fb.png') }}}"></a>
            <a href="http://instagram.com/aelitatomsk/"><img src="{{{ URL::to('images/instagram.png') }}}"></a>
          </div>
        </div>
      </div>
      <div id="copyrights">
        &copy; 2010-2014 г., Зрелищный центр "Аэлита" | Создание сайта и продвижение - <a href="http://aguru.biz/?utm_source=aelita" target="_blank">aguru.biz</a>
        Дизайн сайта - <a href="http://zuzer.ru/" target="_blank">ZuZeR.RU</a>
      </div>
    </footer>
  </body>
</html>
