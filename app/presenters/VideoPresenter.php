<?php

namespace Presenters;

class VideoPresenter extends Presenter
{
  protected $classes = 'video';
  protected $video;
  
  public function __construct($video)
  {
    $this->video = $video;
  }
  
  public function getId()
  {
    return $this->video->video_id;
  }
}
