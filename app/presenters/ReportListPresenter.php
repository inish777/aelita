<?php
namespace Presenters;

class ReportListPresenter extends ListPresenter
{
  private $reports;
  
  public function __construct($reports)
  {
    return $this->reports = $reports;
  }
  
  public function prepare()
  {
    $rows = $this->reports->chunk(3);
    foreach($rows as $row_num => $row) {
      $new_row = ["reports" => [], "classes" => ['reports-row']];
      foreach($row as $report_num => $report) {
        $new_report = new ReportPresenter($report);
        switch($report_num) {
          case 0:
            $new_report->addClass('report-first');
            break;
          case 1:
            $new_report->addClass('report-middle');
            break;
          case 2:
            $new_report->addClass('report-last');
            break;
        }
        
        $new_row["reports"][] = $new_report;
      }
      
      if($row_num == 0) {
        $new_row["classes"][] = "report-row-first";
      }
      else if($row_num == $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "report-row-last";
      }
      else if($row_num % 2 != 0 and $row_num != $rows->count() - 1) {
        $new_row["classes"][] = "report-row-odd";
      }
      else if($row_num % 2 == 0 and $row_num != $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "report-row-even";
      }
      
      $this->rows[] = $new_row; 
    }
  }
}
