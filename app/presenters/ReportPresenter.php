<?php
namespace Presenters;

class ReportPresenter extends Presenter
{
  protected $classes = 'report';
  private $report;
  
  public function __construct($report)
  {
    $this->report = $report;
  }
  
  public function getURLToThumbnail()
  {
    return \URL::to($this->report->thumbnail);
  }
  
  public function getURLToImage()
  {
    return \URL::to($this->report->main_photo);
  }
  
  public function getTitle()
  {
    return $this->report->title;
  }
}
