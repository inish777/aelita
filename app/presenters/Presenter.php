<?php

namespace Presenters;

abstract class Presenter
{
  protected $classes;
  
  public function addClass($class)
  {
    $this->classes .= ' ' . $class;
  }
  
  public function getClasses()
  {
    return $this->classes;
  }
}
