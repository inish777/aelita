<?php
namespace Presenters;

abstract class ListPresenter
{
  protected $rows = [];
  
  public function getRows()
  {
    return $this->rows;
  }
  
  public function getRowClasses($row_num)
  {
    return join($this->rows[$row_num]["classes"], ' ');
  }
}
