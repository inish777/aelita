<?php

namespace Presenters;
class GalleryPhotoPresenter extends Presenter
{
  protected $classes = 'gallery-photo';
  private $photo;
  
  public function getURLToPhotoThumbnail()
  {
    return \URL::to($this->photo->thumbnail_path);
  }
  
  public function getURLToPhoto()
  {
    return \URL::to($this->photo->main_path);
  }
  
  public function getTitle()
  {
    return $this->photo->title;
  }
  
  public function __construct($photo)
  {
    $this->photo = $photo;
  }
}
