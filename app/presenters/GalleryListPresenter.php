<?php

namespace Presenters;

class GalleryListPresenter extends ListPresenter
{
  private $pager;
  
  /**
   *  Prepare galleries list grid for rendering in template
   *  @param Pager $gallery_page
  */
  public function __construct($gallery_page)
  {
    $this->pager = $gallery_page;
  }
  
  public function prepare()
  {
    $rows = $this->pager->chunk(3);
    foreach($rows as $row_num => $row) {
      $new_row = ["galleries" => [], "classes" => ['gallery-row']];
      foreach($row as $gallery_num => $gallery) {
        $new_gallery = new GalleryPresenter($gallery);
        switch($gallery_num) {
          case 0:
            $new_gallery->addClass('gallery-first');
            break;
          case 1:
            $new_gallery->addClass('gallery-middle');
            break;
          case 2:
            $new_gallery->addClass('gallery-last');
            break;
        }
        
        $new_row["galleries"][] = $new_gallery;
      }
      
      if($row_num == 0) {
        $new_row["classes"][] = "gallery-row-first";
      }
      else if($row_num == $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "gallery-row-last";
      }
      else if($row_num % 2 != 0 and $row_num != $rows->count() - 1) {
        $new_row["classes"][] = "gallery-odd";
      }
      else if($row_num % 2 == 0 and $row_num != $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "gallery-even";
      }
      
      $this->rows[] = $new_row; 
    }
  }
  
  public function getPager()
  {
    return $this->pager;
  }
}
