<?php

namespace Presenters;
class VideoListPresenter extends ListPresenter
{
  private $videos;
  
  /**
   * @param $videos Pager
   */
  public function __construct($videos)
  {
    $this->videos = $videos;
  }
  
  public function prepare()
  {
     $rows = $this->videos->chunk(2);
     
     foreach($rows as $row_num => $row) {
      $new_row = ["videos" => [], "classes" => ['video-row']];
      foreach($row as $video_num => $video) {
        $new_video = new VideoPresenter($video);
        switch($video_num) {
          case 0:
            $new_video->addClass('video-first');
            break;
          case 1:
            $new_video->addClass('video-last');
            break;
        }
        
        $new_row["videos"][] = $new_video;
      }
      
      if($row_num == 0) {
        $new_row["classes"][] = "video-row-first";
      }
      else if($row_num == $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "video-row-last";
      }
      else if($row_num % 2 != 0 and $row_num != $rows->count() - 1) {
        $new_row["classes"][] = "video-row-odd";
      }
      else if($row_num % 2 == 0 and $row_num != $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "video-row-even";
      }
      
      $this->rows[] = $new_row; 
    }
  }
  
  public function getPager()
  {
    return $this->videos;
  }
}
