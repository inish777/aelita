<?php

namespace Presenters;

class GalleryPresenter extends Presenter
{
    protected $classes = 'gallery';
    private $gallery;
    
    public function __construct($gallery)
    {
        $this->gallery = $gallery;
    }
    
    public function getGallery()
    {
      return $this->gallery;
    }
    
    public function getURLToMainPhotoThumbnail()
    {
      return \Url::to($this->gallery->main_photo_thumbnail_path);
    }
    
    public function getGalleryId()
    {
      return $this->gallery->id;
    }
    
    public function getTitle()
    {
      return $this->gallery->title;
    }
}
