<?php

namespace Presenters;

class GalleryPhotoListPresenter extends ListPresenter
{
  private $photos;
  
  /**
   * @param $photos Collection
   */
  public function __construct($photos)
  {
    $this->photos = $photos;
  }
  
  public function prepare()
  {
     $rows = $this->photos->chunk(3);
     
     foreach($rows as $row_num => $row) {
      $new_row = ["photos" => [], "classes" => ['gallery-photo-row']];
      foreach($row as $photo_num => $photo) {
        $new_photo = new GalleryPhotoPresenter($photo);
        switch($photo_num) {
          case 0:
            $new_photo->addClass('gallery-photo-first');
            break;
          case 1:
            $new_photo->addClass('gallery-photo-middle');
            break;
          case 2:
            $new_photo->addClass('gallery-photo-last');
            break;
        }
        
        $new_row["photos"][] = $new_photo;
      }
      
      if($row_num == 0) {
        $new_row["classes"][] = "gallery-photo-row-first";
      }
      else if($row_num == $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "gallery-photo-row-last";
      }
      else if($row_num % 2 != 0 and $row_num != $rows->count() - 1) {
        $new_row["classes"][] = "gallery-photo-row-odd";
      }
      else if($row_num % 2 == 0 and $row_num != $rows->count() - 1 and $row_num != 0) {
        $new_row["classes"][] = "gallery-photo-row-even";
      }
      
      $this->rows[] = $new_row; 
    }
  }
}
