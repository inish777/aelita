<?php

class Report extends BaseModel
{
  protected $rules = ['image' => ['required', 'image']];
  protected $table = 'reports';
  
  public function saveImage($image)
  {
    $ds = DIRECTORY_SEPARATOR;
    $report_photo_path = "uploads" . $ds . "report_photos";
    if(!File::exists(public_path() . $ds . $report_photo_path))
        File::makeDirectory(public_path() . $ds . $report_photo_path);
    $uuid = uniqid();
    while(File::exists(public_path() . $ds . $report_photo_path . $ds . $uuid))
        $uuid = uniqid();
    $main_dir = $report_photo_path . $ds . $uuid;
    File::makeDirectory(public_path() . $ds . $main_dir);
    
    //save original photo
    File::makeDirectory(public_path() . $ds . $main_dir . $ds . "original");
    $filename = $image->getClientOriginalName();
    $original_file_path = $main_dir . $ds . "original" . $ds . $filename;
    $image->move(public_path() . $ds . $main_dir . $ds . "original",  $original_file_path);
    $this->main_photo = $original_file_path;

    //save thumbnail
    $thumbnail = InterventionImage::make(public_path() . $ds . $original_file_path)->fit(220, 150, function($constraint) {
        $constraint->upsize();
    });
    File::makeDirectory(public_path() . $ds . $main_dir . $ds . "thumbnail");
    $thumbnail_file_path = $main_dir . $ds . "thumbnail" . $ds . $filename;
    $thumbnail->save(public_path() . $ds . $thumbnail_file_path);
    $this->thumbnail = $thumbnail_file_path;
  }
  
  public function getURLToThumbnail()
  {
    return URL::to($this->thumbnail);
  }
  
  public function getURLToImage()
  {
    return URL::to($this->main_photo);
  }
}
