<?php

class Page extends BaseModel
{
    protected $table = 'pages';
    protected $rules = array(
        "title" => "required",
    );
    
    public function getIDForUrl()
    {
      if(!empty($this->url))
        return $this->url;
      else
        return $this->id;
    }
}
