<?php

class Gallery extends BaseModel
{
    protected $table = "galleries";
    protected $guarded = ["id"];
    protected $fillable = ["title", "description"];

    public $rules = [ "title" => "required", "main_photo" => "image" ];

    public function photos()
    {
        return $this->hasMany("GalleryPhoto", "gallery_id");
    }

    public function saveImage($image)
    {
        $ds = DIRECTORY_SEPARATOR;
        $main_photo_path = "uploads" . $ds . "gallery_main_photos";
        if(!File::exists(public_path() . $ds . $main_photo_path))
            File::makeDirectory(public_path() . $ds . $main_photo_path, 0777, true);
        $uuid = uniqid();
        while(File::exists(public_path() . $ds . $main_photo_path . $ds . $uuid))
            $uuid = uniqid();
        $main_dir = $main_photo_path . $ds . $uuid;
        File::makeDirectory(public_path() . $ds . $main_dir);
        
        //save original photo
        File::makeDirectory(public_path() . $ds . $main_dir . $ds . "original");
        $filename = $image->getClientOriginalName();
        $original_file_path = $main_dir . $ds . "original" . $ds . $filename;
        $image->move(public_path() . $ds . $main_dir . $ds . "original", $original_file_path);
        $this->main_photo_path = $original_file_path;

        //save thumbnail
        $thumbnail = InterventionImage::make(public_path() . $ds . $original_file_path)->fit(220, 150, function($constraint) {
            $constraint->upsize();
        });
        File::makeDirectory(public_path() . $ds . $main_dir . $ds . "thumbnail");
        $thumbnail_file_path = $main_dir . $ds . "thumbnail" . $ds . $filename;
        $thumbnail->save(public_path() . $ds . $thumbnail_file_path);
        $this->main_photo_thumbnail_path = $thumbnail_file_path;
    }
}
