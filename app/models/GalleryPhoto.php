<?php

class GalleryPhoto extends \BaseModel
{
    protected $table = "gallery_photos";
    
    protected $guarded = ["id", "gallery_id"];
    protected $fillable = ["title"];

    protected $rules = ["title" => "required", "photo" => ["required", "image"]];

    public function gallery()
    {
        return $this->belongsTo('Gallery', 'gallery_id');
    }

    public function saveImage($image)
    {
        $ds = DIRECTORY_SEPARATOR;
        $gallery_photo_path = "uploads" . $ds . "gallery_photos";
        if(!File::exists(public_path() . $ds . $gallery_photo_path))
            File::makeDirectory(public_path() . $ds . $gallery_photo_path);
        $uuid = uniqid();
        while(File::exists(public_path() . $ds . $gallery_photo_path . $ds . $uuid))
            $uuid = uniqid();
        $main_dir = $gallery_photo_path . $ds . $uuid;
        File::makeDirectory(public_path() . $ds . $main_dir);
        
        //save original photo
        File::makeDirectory(public_path() . $ds . $main_dir . $ds . "original");
        $filename = $image->getClientOriginalName();
        $original_file_path = $main_dir . $ds . "original" . $ds . $filename;
        $image->move(public_path() . $ds . $main_dir . $ds . "original",  $original_file_path);
        $this->main_path = $original_file_path;

        //save thumbnail
        $thumbnail = InterventionImage::make(public_path() . $ds . $original_file_path)->fit(220, 150, function($constraint) {
            $constraint->upsize();
        });
        File::makeDirectory(public_path() . $ds . $main_dir . $ds . "thumbnail");
        $thumbnail_file_path = $main_dir . $ds . "thumbnail" . $ds . $filename;
        $thumbnail->save(public_path() . $ds . $thumbnail_file_path);
        $this->thumbnail_path = $thumbnail_file_path;
    }
}