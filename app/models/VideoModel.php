<?php

class VideoModel extends BaseModel
{
  protected $rules = ['url' => ['required', 'url']];
  protected $table = 'videos';
  
  public function getIDFromURL($url)
  {
    $youtube = new \Madcoda\Youtube(
      [
        'key' => 'AIzaSyA137CCa61HCsMDb8W63F24ot1wECTI-DM',
      ]
    );
    
    $this->video_id = $youtube->parseVIdFromURL($url);
  }
}
