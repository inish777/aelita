<?php

class Banner extends \BaseModel
{
  protected $table = 'banner';

  protected $rules = ["image" => ["required", "image"], "url" => "url", "order" => "integer"];


  public function saveImage($image)
  {
    $ds = DIRECTORY_SEPARATOR;
    $pp = public_path();

    $banner_path = "uploads" . $ds . "banners";
    if(!File::exists($pp . $ds . $banner_path))
        File::makeDirectory($pp . $ds . $banner_path);
    $uuid = uniqid();
    while(File::exists($pp . $ds . $banner_path . $ds . $uuid))
        $uuid = uniqid();
    $main_dir = $banner_path . $ds . $uuid;
    File::makeDirectory($pp . $ds . $main_dir);
    
    $filename = $image->getClientOriginalName();
    
    //save thumbnail
    $banner = InterventionImage::make($image)->widen(210, function($constraint) {
        $constraint->upsize();
    });
    $banner_path = $main_dir . $ds . $filename;
    $banner->save($pp . $ds . $banner_path);
    $this->image = $banner_path;
  }
}