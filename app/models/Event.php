<?php

class Event extends BaseModel
{
    protected $table = 'events';
    
    protected $fillable = array("title", "description", "price", "time", "date");
    protected $guarded = array("id");

    protected $rules = array(
        "title" => "required",
        "description" => "required",
        "price" => "required",
        "time" => "required",
        "date" => "required|date",
        "icon" => "image",
        "main-photo" => "image",
    );

    public function photos()
    {
        return $this->hasMany('EventPhoto');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date("Y-m-d", strtotime($value));
    }


    public function getDay()
    {
        return $this->date->formatLocalized('%d');
    }

    public function getMonth()
    {
        return Lang::get("locale.month." . $this->date->formatLocalized('%B'));
    }

    public function saveSmallPhoto($photo)
    {
        $small_photos_path = "uploads" . DIRECTORY_SEPARATOR . "event_small_photos";
        $uuid = uniqid();
        while(File::exists(public_path() . DIRECTORY_SEPARATOR . $small_photos_path . DIRECTORY_SEPARATOR . $uuid))
            $uuid = uniqid();
        $small_photo_path = $small_photos_path . DIRECTORY_SEPARATOR . $uuid;
        $this->makeDirectory(public_path() . DIRECTORY_SEPARATOR . $small_photo_path);
        
        $filename = $photo->getClientOriginalName();
        
        $thumbnail = InterventionImage::make($photo)->fit(80, 80, function($constraint) {
            $constraint->upsize();
        });

        $thumbnail->save(public_path() . DIRECTORY_SEPARATOR . $small_photo_path . DIRECTORY_SEPARATOR . $filename);
        $this->icon = $small_photo_path . DIRECTORY_SEPARATOR . $filename;
    }

    public function saveMainPhoto($photo)
    {
        $main_photos_path = "uploads" . DIRECTORY_SEPARATOR . "event_main_photos";
        $uuid = uniqid();
        while(File::exists(public_path() . DIRECTORY_SEPARATOR . $main_photos_path . DIRECTORY_SEPARATOR . $uuid))
            $uuid = uniqid();
        $main_photo_path = $main_photos_path . DIRECTORY_SEPARATOR . $uuid;
        $this->makeDirectory($main_photo_path);
        
        $original_file_path = $main_photo_path . DIRECTORY_SEPARATOR . "original";
        $thumbnail_file_path = $main_photo_path . DIRECTORY_SEPARATOR . "thumbnail";
        $this->makeDirectory(public_path() . DIRECTORY_SEPARATOR . $original_file_path);
        $this->makeDirectory(public_path() . DIRECTORY_SEPARATOR . $thumbnail_file_path);
        
        $filename = $photo->getClientOriginalName();
        
        $original_file = $photo->move(public_path() . DIRECTORY_SEPARATOR . $original_file_path, $filename);
        
        $thumbnail = InterventionImage::make($original_file->getRealPath())->resize(700, null, function($constraint) {
            $constraint->upsize();
            $constraint->aspectRatio();
        });
        $thumbnail->save(public_path() . DIRECTORY_SEPARATOR . $thumbnail_file_path . DIRECTORY_SEPARATOR . $filename);
        $this->main_photo_thumbnail = $thumbnail_file_path . DIRECTORY_SEPARATOR . $filename;
        $this->main_photo = $original_file_path . DIRECTORY_SEPARATOR . $filename;
    }

    private function makeDirectory($path, $mode = 0777)
    {
        if(!File::exists($path))
            File::makeDirectory($path, $mode, true);
    }

    public function getDates()
    {
        return array('created_at', 'updated_at', 'date');
    }

    public function getDescriptionAsText()
    {
        return html_entity_decode(strip_tags($this->description));
    }
}
