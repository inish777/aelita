<?php

class BaseModel extends Eloquent
{
    protected $rules;
    public $validator;

    public function validate($input)
    {
        $this->validator = Validator::make($input, $this->rules);
        return $this->validator->passes();
    }
}