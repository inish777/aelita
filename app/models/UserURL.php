<?php

class UserURL extends BaseModel
{
  protected $rules = [
    'url' => 'required',
    'url_text' => 'required'
  ];
  
  protected $table = 'urls';
}
